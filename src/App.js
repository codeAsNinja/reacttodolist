import React, {
  useState,
  useEffect
} from 'react';
import './App.css';



import Task from './Task/Task'

function App() {

  const [task, setTast] = useState([]);
  const [show, setShow] = useState(0);
  let storedData;
  useEffect(() => {

    storedData = JSON.parse(localStorage.getItem('Task'));
   if(storedData){
    setTast(storedData)
    let count=0;
    for(let i=0;i<storedData.length;i++){
      if(storedData[i].checked === true){
       count=count+1;
      }
    }
    
    setShow(count)
   }
   
  },[])
 
  

  let createTaskHandler = () => {
    let textValue = document.getElementById('task-test').value;
    document.getElementById('task-test').value = "";
    let updatedtask = task.filter(function (item) {
      return (item.name.toLowerCase() !== textValue.toLowerCase())
    })
    updatedtask.push({
      name: textValue,
      taskname: textValue,
      checked: false,

    });
    setTast(
      updatedtask
    );
    localStorage.setItem('Task', JSON.stringify(updatedtask));

  }
  let clickHandler = (e) => {

    let currentArray = task.filter(function (item) {
      return (item.name.toLowerCase() === e.target.name.toLowerCase())
    })
    let oldArray = task.filter(function (item) {
      return (item.name.toLowerCase() !== e.target.name.toLowerCase())
    })

    let newTask = currentArray[0];
   
    newTask.checked = !currentArray[0].checked;
    setTast([...oldArray, newTask]);
    newTask.checked === true ? setShow(show + 1) : setShow(show - 1)

    localStorage.setItem('Task', JSON.stringify([...oldArray, newTask]));
  }
 
  let taskUi = task.map((taskk, key) => {

    if (taskk.checked === false) {
      return <Task
      key = {
        key
      }
      name = {
        taskk.name
      }
      checked = {
        taskk.checked
      }
      onChange = {
        clickHandler
      }
      taskname = {
        taskk.name
      }
      />
    }

  });
  let completedTask = task.map((taskk, key) => {
    if (taskk.checked === true) {
      return <Task
      key = {
        key
      }
      name = {
        taskk.name
      }
      checked = {
        taskk.checked
      }
      onChange = {
        clickHandler
      }
      taskname = {
        taskk.name
      }
      />
    }
  })
    return (
      <div className="App">
        <p>Task By Nitish</p>
        <input type="text" autoComplete="off" placeholder="Type To do Item" className="App-input" id="task-test"></input>
        <button onClick={createTaskHandler} className="App-button">Submit</button>
        <div>
          {taskUi}
        </div>
        <div id="hideIfNull" style={
         {display:show!==0 ? 'block' : 'none'}
        }>
          <p className="App-completed">Completed Task</p>
          {completedTask}
        </div>
         
      </div>
    );
  }

export default App;
