import React from 'react';
import PropTypes from 'prop-types';

import './Task.css';
const task = (props) => {
    return(
        <div className="Task-checkbox">
         <label>
         <input type="checkbox" name={props.name} checked={props.checked} onChange={props.onChange}/>
                {props.taskname}
        </label>
        </div>
    );
}

task.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string.isRequired,
    checked: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
  }
export default task